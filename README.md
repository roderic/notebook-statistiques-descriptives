# TD de statistiques descriptives

L'ennoncé et le corrigé sont dans [stat_desc.ipynb](stat_desc.ipynb) sous forme de [jupyter notebook](jupyter.org).
Le fichier de données est [Processed.3d](Processed.3d).

## Utilisation du notebook

Installer jupyter notebook ou jupyter lab en suivant [la documentation](https://jupyter.org/install).

Télécharger le fichier ipynb et le fichier de données. Lancer un environnement au choix :
- notebook
```
jupyter-notebook stat_desc.ipynb
```
- lab
```
jupyter-lab stat_desc.ipynb
```
